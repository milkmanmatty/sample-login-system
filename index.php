<?php
	include "scripts/template.php";
	include_once "scripts/defs.php";
	
	session_start(); // start or resume session

//Set user access to false
$userAccess = false;

//Define some variables
$error = "";
$alert = "";
$current_user = "";
$inputUsername = "";
$ref = "main.php";

#Get login error data
if(isset($_GET['error'])){
	$error = $_GET['error'];
}
if(isset($_GET['alert'])){
	$alert = $_GET['alert'];
}

# Get user data.
if(isset($_SESSION['current_user'])){
	$current_user = $_SESSION['current_user'];
	if(is_array($current_user)){
		//If it is, grab the username from the array. This user has logged on.
		$inputUsername = ucfirst($current_user['username']);
		$userAccess = true;
	} else {
		//If it isn't an array then this user just failed to login. Repeat username in approrpriate form.
		$inputUsername = $current_user;
	}
}

//If no userAccess then get user out of here!
if($userAccess == false){
	
	//Kick em out
	$ref = "index.php";
	header("Location: login.php?ref=$ref");
	exit;
}
/***** END LOGIN *****/
	
	
?>
<?php print getHTMLHead(); ?>

<body>
    <div class="site-wrapper">
		<div class="site-wrapper-inner">
			<div class="cover-container">

				<?php print getNav($inputUsername); ?>
				<div id="inset">
					<?php if($alert){ ?>
						<span id="alert"><?php echo $alert ?></span>
					<?php } ?>
				</div>
				<div class="inner cover">
					<h1 class="cover-heading">Welcome, <?php echo"$inputUsername"; ?></h1>
					<p class="lead">Sample logged in content. This is placeholder text demonstrating what would be shown to a user that has logged in.</p>
					<p class="lead">
						<a href="#" class="btn btn-lg btn-secondary">Learn more</a>
					</p>
				</div>
				
				<?php print getFooter(); ?>
			
			</div>
		</div>
	</div>
	<?php print getScripts(); ?>
</body>
</html>