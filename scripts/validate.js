/*	form_validation.js
**	Contains functions for form validation. Also provides a way to send form data to a php script.
**	Commenting may be excessive, try to limit.
*/

window.onload = function() {
	//If a value has already been entered.
	if(typeof document.register != "undefined"){
		if(document.register.Username.value){
			validate(true, false);
		} 
	}
};


function activateButton(){
	if(typeof document.register != "undefined"){
		if(validate(true)){
			document.register.button.disabled = false;
		}
	}
}

/*	checkField(field, silent, showEmpty)
**	Verifies data integrity of a single field.
**	First, it checks to make sure the field is not empty.
**	param silent determines whether or not to change any data/element in the DOM.
**	param showEmpty determines whether or not to display a space character for ease of viewing if a field validates properly.
**	Returns:	true if the field is non-empty and the second validation is either skipped or successful, false otherwise.
*/
function checkField(field, silent, showEmpty){
	var value = field.value.trim();
	
	//Check to see if the field is empty.
	if (value == "") {
		if(!silent){
			//If it is, show message to user and focus on the field.
			document.getElementById(field.name+"req").innerHTML = 'Please enter a non-empty value for '+field.name+'.';
			field.focus();
		}
		//Returns false, function stops here.
		return false;
	} else {
		if(!silent){
			if(showEmpty){
				document.getElementById(field.name+"req").innerHTML = '&nbsp;';
			} else {
				document.getElementById(field.name+"req").innerHTML = '';
			}
		}
	}
	
	//If first check was successful, see if correct characters are present. Exclude Password & email.
	//If a match is found take appropriate actions. Regex is magical.
	if(field.name == "Username") {
		var name = value;
		var alphanumeric = /^[a-zA-Z0-9]+$/;
		
		//If it didn't validate properly
		if (!alphanumeric.test(name)) {
			if(!silent){
				//Change the error field to display a message.
				document.getElementById(field.name+"req").innerHTML = 'Please enter a valid '+field.name;
			}
			return false;
		} else {
			if(!silent){
				if(showEmpty){
					document.getElementById(field.name+"req").innerHTML = '&nbsp;';
				} else {
					document.getElementById(field.name+"req").innerHTML = '';
				}
			}
		}
	}
	
	//If all went well return true :D
	return true;
}

/*	validate(silent)
**	Checks all fields from register.html for valid data.
**	param silent determines whether or not to change any data/element in the DOM. Also specifies whether or not to return true or to submit data to PHP file. Used when making the submit button available.
**	Returns:	nothing if all fields succesfully validated their data, false otherwise.
*/
function validate(silent) {
	
	if (typeof silent == "undefined" || typeof silent == "null"){
		return false;
	}

	//Setup variables with the boolean value of their respective checkField() return value.
	var username 	= checkField(document.register.Username, silent, true);		//Username
	var email 		= checkField(document.register.email, silent, true);		//Email
	var password 	= checkField(document.register.password, silent, true);		//Password
	
	//If all passes then send data to php.
	if (username && email && password) {
		if(!silent){
			//Make action go somewhere and then actually submit data.
			document.register.action = "scripts/add_user_action.php";
			document.register.submit();
		} else {
			return true;
		}
	}
	
	//Otherwise return false.
	return false;
}
