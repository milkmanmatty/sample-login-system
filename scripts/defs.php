<?php
/*	defs.php
**	This is the main definition script. It incorporates all of the previously fractured definition files.
**  This script contains auxiliary function definitions and all of the globals.
**	Functions are listed alphabetically by function name for ease of viewing.
**	Functions may have documentation comments prefixed before declaration, however if they are assummed to be obvious then documentation may be missing.
**	
**	Now also includes all functions that were originally from defs_am.php as well.
**	This script now contains all of the accessor and mutator function definitions.
**
**	Also includes all functions that were originally from defs_sql.php
**	This script now contains all of the function definitions relating to SQL querying.
**
**	Also includes all functions that were originally from defs_verify.php
**	This script now contains all of the php function definitions relating to input sanitising.
**
**	PRO-TIP: Search for caps when looking for function definition.
**
**	Matthew Stubbins, s2757779
*/

/* The following information block would normally be in it's own file, out of public visibility */
define("HOST", "test.site.domain"); // The database server's host
define("USER", "123456_db"); // The database owner's name
define("PASSWORD", "password123"); // The database owner's password
define("DATABASE", "123456_db"); // The database's name
 
function show_error($query=null) {
	if(is_null($query)){
		$query = "(not provided)";
	}
	die("Error ". mysql_errno() . " : " . mysql_error() . "<br />Query: ".$query);
}
function mysql_open() {
	$con = @mysqli_connect(HOST, USER, PASSWORD, DATABASE);
	if(!$con){
		echo "Error: Unable to connect to MySQL." . PHP_EOL;
		echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
		echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
		exit;
	}
	return $con;
}

// ADD USER
/* Adds a user to the SQL Table user.
** Note that this function calls testCreatation() everytime it is invoked. 
** This is nesseccary to ensure that the SQL Table users exists however repeated calls of this function waste resources invoking the testCreatation() function.
** A better invoking structure must exist however currently eludes me.
**
** Should the SQL query fail to execute or close an (ugly) error message is provided.
** Returns the id of the user created.
*/
function add_user($username, $email, $password) {
    $username = mysql_real_escape_string($username);
	$username = strtolower($username);
    $email = mysql_real_escape_string($email);
	$password = mysql_real_escape_string($password);
	
	$salt = substr($username, 0, 2);
	$seasonedPassword = crypt($password, $salt);
	
	$query = "INSERT INTO users (id, username, email, password) VALUES(NULL, '$username', '$email', '$seasonedPassword')";

    return perform_SQL_int($query);
}

//AUTHENTICATE
/* Returns TRUE if $username/$password matches, else FALSE. */
function authenticate($username, $password) {
	if(empty($username) || empty($password)){
		return FALSE;
	}
	$username = strtolower($username);
	$salt = substr($username, 0, 2);
	$seasonedPassword = crypt($password, $salt);
	$connection = mysql_open();
	$query = "SELECT * FROM users WHERE username = '$username' AND password = '$seasonedPassword'";
	$result = mysql_query($query, $connection) or show_error();
	mysql_close($connection) or show_error();
	return (mysql_num_rows($result) == 1);
}

//GET USER NAME FROM USERNAME
/* Constructs an SQL query to obtain a matching user from the provided username from the table users. Then uses the perform_SQL_array() function to execute the query.
** Returns the array provided from perform_SQL_array() containing an array of all of the data from the matched user.
** If the resulting array obtains by perform_SQL_array() is empty, 1 is returned.
*/
function get_userFromUsername($username) {

    $query = "SELECT id, username FROM users WHERE username = '$username'";
    $results = perform_SQL_array($query);
	
	if(empty($results)){
		$results = 1;
	}
	return $results;
}

//LOGIN
/*	Attempts to log a user in using hte provided username and password.
**	Optional paramater headerRequests specifies whether or not changes to the header are made.
**	Optional paramater loc specifies where the user is sent if headerRequests is set to true AND the authentication doesn't fail.
**	Returns nothing.
*/
function login($sentUsername, $sentPassword, $headerRequests=true, $loc=null){
	// Initialise a session. This call either creates a new session or re-establishes an existing one.
	session_start();

	// get session id
	$sessionId = session_id();

	//set referer default
	$referer = removeQuery($_SERVER['HTTP_REFERER']);

	//If username/password are empty
	if(empty($sentUsername) || empty($sentPassword)) {
		$_SESSION['current_user'] = "".$_POST['username'];
		$error = "Please fulfill both Username and Password.";
		if($headerRequests){
			header("Location: $referer?error=$error");
			exit;
		} else {
			return;
		}
	} 
	
	$sentUsername = strtolower($sentUsername);

	//Current User. Hopefully not set
	$curUser = "";
	if(isset($_SESSION['current_user'])){
		$curUser = $_SESSION['current_user'];
	}

	// if this is an array then someone is trying to login twice on the one machine
	if(is_array($curUser)){
		$error = "You are already logged in.";
		if($headerRequests){
			header("Location: $referer?error=$error");
			exit;
		} else {
			return;
		}
	}

	//if matching username/password is found
	if(authenticate($sentUsername, $sentPassword)) {
	
		//Obtain sent referer
		if(!empty($loc)){
			$referer = $loc;
		}
		
		$tempUser = get_userFromUsername($sentUsername);
		$_SESSION['current_user'] = $tempUser[0];
		if($headerRequests){
			header("Location: $referer");
			exit;
		} else {
			return;
		}
		
	//If wrong username/password
	} else {
		$_SESSION['current_user'] = "".$sentUsername;
		$error = "Invalid username/password.";
		if($headerRequests){
			header("Location: $referer?error=$error");
			exit;
		} else {
			return;
		}
	}
}

/* Performs an SQL query that requires an arry of returning values.
** Should the SQL query fail to execute or close, an (ugly) error message is provided.
*/
function perform_SQL_array($query){
	$connection = mysql_open();
	$result = mysql_query($query,$connection) or show_error($query);
	$results = array();
	while ($row = mysql_fetch_array($result)) {
		$results[] = $row;
	}
	return $results;
}


/* Performs an SQL query that requires an int as the returning value.
** Should the SQL query fail to execute or close, an (ugly) error message is provided.
*/
function perform_SQL_int($query){
	$connection = mysql_open();
    $result = mysql_query($query, $connection) or show_error($query);
    $id = mysql_insert_id();
    mysql_close($connection) or show_error($query);
	return $id;
}


/* Performs an SQL query that requires an offsetted array as the returning value.
** Should the SQL query fail to execute or close, an (ugly) error message is provided.
*/
function perform_SQL_page($query, $offset) {
	$connection = mysql_open();

	// Get list of items on this page
	$results = mysql_query($query, $connection) or show_error($query);

	// Get total number of items
	$result2 = mysql_query("SELECT FOUND_ROWS()", $connection) or show_error($query);
	$row = mysql_fetch_array($result2);
	$num_items = $row[0];

	// Copy result set to array
	$items = array();
	while ($row = mysql_fetch_array($results)) {
		$items[] = $row;
	}

	mysql_close($connection) or show_error($query);
	return array($items, $num_items);
}

/* Performs an SQL query that requires no returning value.
** Should the SQL query fail to execute or close, an (ugly) error message is provided.
** Returns true if the SQL is executed without an error.
*/
function perform_SQL($query){
	$connection = mysql_open();
    $result = mysql_query($query, $connection) or show_error($query);
    mysql_close($connection) or show_error($query);
	return true;
}

// REMOVE QUERY
/*	Removes any paramters from a given string and then returns the trimmed string.
**	The provided string is assumed to be a url.
*/
function removeQuery($url) {
	if ($url != '') {
		$queryURL = parse_url(trim($url), PHP_URL_QUERY);
		$fragmentURL = parse_url($url, PHP_URL_FRAGMENT);
		$remove_query_and_fragment = str_replace(array("$queryURL","$fragmentURL"), '', $url);
		$trimmed_host_and_paths = rtrim($remove_query_and_fragment, '?,#,/');
		
		return $trimmed_host_and_paths;
	} else {
		return "no url inserted";
	}
}

?>