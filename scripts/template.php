<?php

	/* Provides the code for the <head></head> section of the HTML document.
	** The title is passed in and if provided, used. Otherwise a default title is used.
	** The SEO desctription, author, favicon and stylesheets are defined here.
	*/
	function getHTMLHead($title=null){
		$r = '<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="An example of a simple login system">
	<meta name="author" content="Milkman Matty">
	<link rel="shortcut icon" href="../images/favicon.png" />
	';
	if($title){
		$r .= "<title>$title</title>";
	} else {
		$r .= '<title>Sample Login System</title>';
	}
	$r .= '
	<!-- Add Bootstrap first -->
	<link href="styles/bootstrap.min.css" rel="stylesheet">

	<!-- Apply Custom-Sytlesheet over the top -->
	<link href="styles/style.css" rel="stylesheet">
</head>';
		return $r;
	}
	
	/* Provides the code for the navigation.
	*/
	function getNav($user=null){
		$r = '
				<div class="sls clearfix">
					<div class="inner">
						<h3 class="sls-brand">Cover</h3>
						<nav class="nav nav-sls">
							<a class="nav-link active" href="#">Home</a>
						</nav>
					</div>
				'.getLogout($user).'
				</div>';
		return $r;
	}
	
	
	/* 
	** 
	** 
	*/
	function getLogout($user=null){
	
		if($user != null){
		
			$r = '
		<div id="logout-container">
			<div id="logout-pos">
				<form method="post" action="scripts/logout_action.php">
					<fieldset id="logout">
						<span id="username">You are logged in as: <span id="user">'.$user.'</span> - <input type="submit" value="Logout"></span>
					</fieldset>
				</form>
			</div>
		</div>';
			
		} else {
			$r = '
		<div id="logout">
			<div id="logout-pos">
				<span id="username">You are not logged in</span>
			</div>
		</div>';
		}
		
		return $r;
	}
	
	
	/* Provides the code for the footer.
	*/
	function getFooter(){
		$r = '<footer>
					<div class="inner">
						<p>Sample Login System written by <a href="httpp://www.milkmanmatty.com">Milkman Matty</a>.</p>
					</div>
				</footer>';
		return $r;
	}

	/* Provides the code for the Scripts used within the website.
	** These are placed at the end of the page for faster loading for the client.
	*/
	function getScripts(){
		$r = '<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
	<script>window.jQuery || document.write(\'<script src="scripts/jquery.min.js"><\/script>\')</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
	<script src="scripts/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="scripts/ie10-viewport-bug-workaround.js"></script>
		';
		return $r;
	}	
?>