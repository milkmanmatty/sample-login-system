<?php
/*	Calls Login */
require_once "defs.php";

// Initialise a session. This call either creates a new session or re-establishes an existing one.
session_start();

// get session id
$sessionId = session_id();

//Obtain trimmed referer. This will be used to specify where to send user once they have logged in.
$referer = removeQuery($_SERVER['HTTP_REFERER']);
if(isset($_GET['ref'])){
	$referer = $_GET['ref'];
}

//Hopefully set, but initialize as null string just in case
$sentUsername = "";
$sentPassword = "";
if(isset($_POST['username'])){
	$sentUsername = $_POST['username'];
}
if(isset($_POST['password'])){
	$sentPassword = $_POST['password'];
}

if(empty($sentUsername) || empty($sentPassword)) {
	$_SESSION['current_user'] = $_POST['username'];
	$error = "Please fulfill both Username and Password.";
	
	//Redirect to the page that the login failed on. Do not use $_GET[ref].
	$referer = removeQuery($_SERVER['HTTP_REFERER']);
	
	header("Location: $referer?error=$error");
	exit;
} else {
	if(isset($_GET['ref'])){
		$referer = $_GET['ref'];
		login($sentUsername, $sentPassword, true, "../".$referer);
	} else {
		login($sentUsername, $sentPassword, true, "../index.php");
	}
}

?>