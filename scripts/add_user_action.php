<?php
/*
 * Adds new user from form data.
 */

include_once "../includes/defs.php";

//Define variables
$username = "";
$email = "";
$password = "";

# Get form data. Because code replication is fun :@
if(isset($_POST['Username'])){
	$username = $_POST['Username'];
}
if(isset($_POST['email'])){
	$email = $_POST['email'];
}
if(isset($_POST['password'])){
	$password = $_POST['password'];
}

//check for duplicate username
$bug = get_userFromUsername($username);
if($bug != 1){
	$params = "username=$username&email=$email&a=Username+already+taken.+Please+select+a+different+Username.";
	header("Location: ../register.php?$params"); 
}

# add new user with form data
$id = add_user($username,$email,$password);

//login($username, $password, false);

header("Location: ../index.php?id=$id"); 
exit;
?>
