<?php
/* Handles users loging out
*/

// Initialise a session. This call either creates a new session or re-establishes an existing one.
session_start();

//Obtain referer
$referer = $_SERVER['HTTP_REFERER'];

//Unset session, destroy session, set session cookie to nothing, set $_session array to nothing.
session_unset();
session_destroy();
setcookie("PHPSESSID", "", 1);
$_SESSION = array();

//Return to sender
header("Location: $referer");
?>