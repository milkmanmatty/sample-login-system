<?php
	include "scripts/template.php";

session_start(); // start or resume session

//Set user access to false
$userAccess = false;

//obtain trimmed referer. This will be used in the script login_action.php
$referer = "../index.php";
if(isset($_GET['ref'])){
	$referer = $_GET['ref'];
}

//Define some variables
$alert = "";
$error = "";
$current_user = "";
$inputUsername = "";

#Get login error data
if(isset($_GET['a'])){
	$alert = $_GET['a'];
}
if(isset($_GET['error'])){
	$error = $_GET['error'];
}

# Get user data.
if(isset($_SESSION['current_user'])){
	$current_user = $_SESSION['current_user'];
	if(is_array($current_user)){
		//If it is an array, grab the username from the array. This user has logged on.
		$inputUsername = ucfirst($current_user['username']);
		$userAccess = true;
	} else {
		//If it isn't an array then this user just failed to login.
	}
}
/***** END LOGIN *****/
	
?>

<?php print getHTMLHead(); ?>

<body>
    <div class="site-wrapper">
		<div class="site-wrapper-inner">
			<div class="cover-container">
				<?php print getNav($inputUsername); ?>
				
				<div id="inset">
					<?php if($alert){ ?>
						<span id="alert"><?php echo $alert ?></span>
					<?php } ?>
				</div>
				<div class="inner cover">
					<h1 class="cover-heading">Login page</h1>
					<p class="lead">To access the page you desire you must login using your account. If you haven't got an account you can request an account by contacting test@email.com. Please login below.</p>
					<p class="alert"><?php echo "$error"; ?></p>
					
					<!-- <?php echo $referer; ?> -->
					
					<form class="form-signin" method="post" action="scripts/login_action.php?ref=<?php echo "$referer"; ?>">
						<h2 class="form-signin-heading">Please sign in</h2>
						<label for="username" class="sr-only">Username</label>
						<input type="text" id="username" name="username" class="form-control" placeholder="Username" value="<?php echo "$current_user"; ?>" required autofocus>
						
						<label for="password" class="sr-only">Password</label>
						<input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
						
						<div class="checkbox">
							<label>
								<input type="checkbox" value="remember-me"> Remember me
							</label>
						</div>
						<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
					</form>
				</div>
				
				<?php print getFooter(); ?>
			
			</div>
		</div>
	</div>
	<?php print getScripts(); ?>

</body>
</html>